require 'rails_helper'

RSpec.describe "Users Controller", type: :request do
  # rspec examples and expect keywords
  context "expect Keywords and examples |" do
    it "Expected to be not equal" do
      a = "x" + "y"
      expect(a).not_to eql('xy')
    end
    it "Expected to be equal" do
      expect("sharan").to eql("sha")
    end
    it "Expected to be true" do
      exp = 2 > 3
      expect(exp).to be true
    end
    it "Expected not to be true" do
      exp = 2 > 1
      expect(exp).not_to be true
    end
    it "should show how the comparison Matchers work" do
      a = 1
      b = 2
      c = 3
      d = 'test string'

      # The following Expectations will all pass
      expect(b).to be > a
      expect(a).to be >= a
      expect(a).to be < b
      expect(b).to be <= b
      expect(c).to be_between(1,3).inclusive
      expect(b).to be_between(1,3).exclusive
      expect(d).to match /TEST/i
    end
    # testing two string
    it "should show how the equality Matchers work" do
      a = "test string"
      b = "test string"

      # The following Expectations will all pass
      # expect(a).to eq "test string"
      # expect(a).to eql "test string"
      expect(a).to eq b
      # expect(a).to equal b
    end
  end
end
