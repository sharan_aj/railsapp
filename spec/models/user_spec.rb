require 'rails_helper'

RSpec.describe User, type: :model do
  # test a User model
  it "name should not be nil" do
    user = User.new(name: "sharan")
    expect(user.name).not_to be_nil
  end
end
