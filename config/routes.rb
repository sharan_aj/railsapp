Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "home#index"
  get "/about",to: "about#index"
  get "/users", to: "users#index"
  get "/users/show", to: "users#show"
end
